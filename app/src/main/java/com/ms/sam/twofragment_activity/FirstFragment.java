package com.ms.sam.twofragment_activity;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;


/**
 * A simple {@link Fragment} subclass.
 */
public class FirstFragment extends Fragment {

    private ListView listView;
    private String android[];


    public FirstFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_first, container, false);

        listView=view.findViewById(R.id.listview);

        android=getResources().getStringArray(R.array.version);

        ArrayAdapter<String> adapter=new ArrayAdapter<String>(getActivity(),
                R.layout.first,android);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
            {

                FragmentManager fm=getFragmentManager();
                SecondFragment secondFragment= (SecondFragment) fm.findFragmentById(R.id.fragment3);

                if(i==0)
                {
                    secondFragment.imageView.setImageResource(R.drawable.cupcake);

                }
                if (i==1)
                {
                    secondFragment.imageView.setImageResource(R.drawable.donut);

                }

                if(i==2)
                {
                    secondFragment.imageView.setImageResource(R.drawable.eclair);

                } if(i==3)

            {
                secondFragment.imageView.setImageResource(R.drawable.foryo);

            }
            if(i==4)
            {
                secondFragment.imageView.setImageResource(R.drawable.gingerbread);

            } if(i==5)
            {
                secondFragment.imageView.setImageResource(R.drawable.honeycomb);

            } if(i==6)
            {
                secondFragment.imageView.setImageResource(R.drawable.icecream);

            }


                if(i==7)
                {
                    secondFragment.imageView.setImageResource(R.drawable.jellybean);

                }
                if(i==8)
                {
                    secondFragment.imageView.setImageResource(R.drawable.kitkat);

                }
                if(i==9)
                {
                    secondFragment.imageView.setImageResource(R.drawable.lolipop);

                }
                if(i==10)
                {
                    secondFragment.imageView.setImageResource(R.drawable.marshmallow);

                }
                if(i==11)
                {
                    secondFragment.imageView.setImageResource(R.drawable.nougat);

                }

                if(i==12)
                {
                    secondFragment.imageView.setImageResource(R.drawable.oreo);

                }
                if(i==13)
                {
                    secondFragment.imageView.setImageResource(R.drawable.pancake);

                }




            }
        });


        return view;
    }

}
